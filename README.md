# Frontend Mentor - Product feedback app solution

This is a solution to the [Product feedback app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-feedback-app-wbvUYqjR6). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Create, read, update, and delete product feedback requests
- Receive form validations when trying to create/edit feedback requests
- Sort suggestions by most/least upvotes and most/least comments
- Filter suggestions by category
- Add comments and replies to a product feedback request
- Upvote product feedback requests
- **Bonus**: Keep track of any changes, even after refreshing the browser (`localStorage` could be used for this if you're not building out a full-stack app)

### Screenshot

![](./screenshot.jpg)

Add a screenshot of your solution. The easiest way to do this is to use Firefox to view your project, right-click the page and select "Take a Screenshot". You can choose either a full-height screenshot or a cropped one based on how long the page is. If it's very long, it might be best to crop it.

Alternatively, you can use a tool like [FireShot](https://getfireshot.com/) to take the screenshot. FireShot has a free option, so you don't need to purchase it.

Then crop/optimize/edit your image however you like, add it to your project, and update the file path in the image above.

**Note: Delete this note and the paragraphs above when you add your screenshot. If you prefer not to add a screenshot, feel free to remove this entire section.**

### Links

- Solution URL: [Add solution URL here](https://your-solution-url.com)
- Live Site URL: [Add live site URL here](https://your-live-site-url.com)

## My process

### Built with

- [Next.js](https://nextjs.org/) - React framework
- [Styled Components](https://styled-components.com/) - For styles
- Redux - State Management
- Typescript
- Prisma with PostgreSQL

### Running the app
#### development 
- start postgres db: `docker-compose up`
- Prisma studio: `yarn prisma:studio`
- next.js: `yarn dev`

### What I learned

- [Transient props in styled components](https://styled-components.com/docs/api#transient-props)
In `components/ui/Cards.tsx` there was a warning in console, `app-index.js:33 Warning: React does not recognize the `textColor` prop on a DOM element.`.
That was because the props was passed down the children node


- [Using react-select with react hook form](https://stackoverflow.com/questions/68052566/return-the-value-of-react-select-using-react-hook-form-with-typescript)  


- There are different ways to structure a nextjs app with the app router (https://nextjs.org/docs/app/building-your-application/routing/colocation#project-organization-strategies)
I've decided to "store project files outside of app", and use `app` purely for routing


- Next.js hydration error with react-select: https://github.com/JedWatson/react-select/issues/5459

- [Setting up prisma with nextjs](https://www.sammeechward.com/prisma-and-nextjs) - The main thing here is prisma uses `.env` and nextjs uses `.env.local`, so `dotenv-cli` is used to make prisma use `.env.local`.

- [Using PrimsaClient with Next.js](https://www.prisma.io/docs/guides/other/troubleshooting-orm/help-articles/nextjs-prisma-client-dev-practices)

- Looks like `getServerSideProps` is not working anymore in next13 app router
  - https://stackoverflow.com/questions/76276597/getserversideprops-not-being-called-nextjs-13
  - https://nextjs.org/docs/app/building-your-application/data-fetching/fetching#async-and-await-in-server-components
  - https://nextjs.org/blog/next-13#data-fetching
    ```
    // This request should be refetched on every request.
    // Similar to `getServerSideProps`.
    // fetch(URL, { cache: 'no-store' });
    ```
- With app router, you cannot have async component, otherwise there will be infinite re-render, refre to `src/app/page.tsx`
  https://www.youtube.com/watch?v=zwQs4wXr9Bg&ab_channel=JackHerrington
  https://github.com/vercel/next.js/issues/50382
- Generate a random id with `openssl rand -base64 32`

### Continued development

Use this section to outline areas that you want to continue focusing on in future projects. These could be concepts you're still not completely comfortable with or techniques you found useful that you want to refine and perfect.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [Redux Toolkit Tutorial With Next 13](https://www.youtube.com/watch?v=Yokjzp91A4o)
- [Redux Style Guide](https://redux.js.org/style-guide/)
- [Next-Auth Login Authentication Tutorial with Next.js App Directory](https://www.youtube.com/watch?v=w2h54xz6Ndw&ab_channel=DaveGray)
- https://redux.js.org/tutorials/essentials/part-5-async-logic
- [Nextjs 13 infinite loop](https://www.youtube.com/watch?v=zwQs4wXr9Bg&ab_channel=JackHerrington)
- [State Management for NextJS 13 Server and Client Components](https://www.youtube.com/watch?v=dRLjoT4r-jc&ab_channel=JackHerrington)

**Note: Delete this note and replace the list above with resources that helped you during the challenge. These could come in handy for anyone viewing your solution or for yourself when you look back on this project in the future.**

## Author

- Website - [Add your name here](https://www.your-site.com)
- Frontend Mentor - [@yourusername](https://www.frontendmentor.io/profile/yourusername)
- Twitter - [@yourusername](https://www.twitter.com/yourusername)

**Note: Delete this note and add/remove/edit lines above based on what links you'd like to share.**

## Acknowledgments

This is where you can give a hat tip to anyone who helped you out on this project. Perhaps you worked in a team or got some inspiration from someone else's solution. This is the perfect place to give them some credit.

**Note: Delete this note and edit this section's content as necessary. If you completed this challenge by yourself, feel free to delete this section entirely.**
