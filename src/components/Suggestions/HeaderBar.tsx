"use client"
import Card from '@/components/ui/Card';
import Button from '@/components/ui/Button';
import styled from 'styled-components';
import Image from 'next/image';
import {devices} from '@/styles/sizes';
import Link from 'next/link';

const S = {
    Container: styled.section`
      display: flex;
      justify-content: space-between;
      align-items: center;
      width: 100%;
      gap: 10px;
      
      header {
        display: none;
        
        @media ${devices.tablet} {
          display: flex;
          align-items: center;
        }
      }

      .sortBy {
        font-size: 0.8rem;
        font-weight: lighter;
      }

      select {
        background-color: var(--navy-600);
        color: var(--white);
        border: none;
      }
    `
}

const HeaderBar = () => {
    return(
        <Card background="var(--navy-600)" textColor="var(--white)">
            <S.Container>
                <header>
                    <Image
                        src="/assets/suggestions/icon-suggestions.svg"
                        alt="suggestions-icon"
                        width="24"
                        height="24"
                    />
                    <span>6 Suggestions</span>
                </header>
                <span className="sortBy">Sort by:</span>
                <select>
                    <option>Most Upvotes</option>
                    <option>Least Upvotes</option>
                    <option>Most Comments</option>
                    <option>Least Comments</option>
                </select>
                <Link href="/feedback/new"><Button text="+ Add Feedback"/></Link>
            </S.Container>
        </Card>
    )
 }
 
 export default HeaderBar