"use client"
import Card from '@/components/ui/Card';
import Tag from '@/components/ui/Tag';
import styled from 'styled-components';
import Image from 'next/image';
import {devices} from '@/styles/sizes';
import {Feedback} from "@/types/suggestions";

const S = {
    Container: styled.article`
      display: grid;
      grid-template-areas: 
        "content content"
        "upvote comment"
      ;
      gap: 20px;
      align-items: center;
      justify-content: space-between;
      width: 100%;   
      
      @media ${devices.tablet} {
        display: flex;
        gap: 40px;
      }
            
    `,
    Content: styled.div`
      grid-area: content;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      gap: 5px;
    `,
    Upvote: styled.div`
      grid-area: upvote;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      gap: 5px;
      align-self: flex-start;
      background-color: var(--gray-200);
      border-radius: 5px;
      height: 53px;
      width: 40px;      
      font-weight: bold;
      color: var(--navy-500)
    `,
    CommentCount: styled.div`
      grid-area: comment;
      margin-left: auto;
      display: flex;
      align-items: center;
      gap: 5px;
      font-weight: bold;
    `
}


const UpvoteBtn = ({upvotes}: {upvotes: number}) => {
    return (
        <S.Upvote>
            <Image src="/assets/shared/icon-arrow-up.svg" alt="upvotebutton" width="12" height="8"/>
            <span>{upvotes}</span>
        </S.Upvote>
    )
}

const CommentCount = ({count}:{count: number}) => {
    return(
        <S.CommentCount>
            <Image src="/assets/shared/icon-comments.svg" alt="upvotebutton" width="20" height="18"/>
            <span>{count}</span>
        </S.CommentCount>
    )
}

const SuggestionCard = ({feedback}:{feedback: Feedback}) => {
    return(
        <Card>
            <S.Container>
                <UpvoteBtn upvotes={feedback.upvotes}/>
                <S.Content>
                    <h1>
                        {feedback.title}
                    </h1>
                    <p>
                        {feedback.description}
                    </p>
                    <Tag name="Enhancement"/>
                </S.Content>
                <CommentCount count={5}/>
            </S.Container>

        </Card>
    )
 }
 
 export default SuggestionCard