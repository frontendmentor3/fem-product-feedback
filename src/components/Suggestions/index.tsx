"use client"
import HeaderBar from '@/components/Suggestions/HeaderBar';
import SuggestionCard from '@/components/Suggestions/SuggestionCard';
import styled from 'styled-components';
import {devices} from '@/styles/sizes';
import {useAppSelector} from "@/hooks/redux";


const S = {
    Container: styled.section`
      display: flex;
      flex-direction: column;
      gap: 20px;
      width: 90%;
      
      @media ${devices.desktop} {
        width: 60%;
        min-width: 600px;
      }
    `
}

const Suggestions = async () => {
    const allFeedback = useAppSelector(state=>state.feedback.feedback)
    return(
    <S.Container>
        <HeaderBar/>
        {allFeedback.map( (feedback, i)=> (
            <SuggestionCard key={i} feedback={feedback}/>
        ))}
    </S.Container>
    )
 }
 
 export default Suggestions