"use client"

import {Feedback} from "@/types/suggestions";
import {store} from "@/redux/store";
import {setStartupFeedback} from "@/redux/features/feedbackSlice";
import {useAppSelector} from "@/hooks/redux";

function Preloader({feedback}:{feedback:Feedback[]}) {
    const fetched = useAppSelector(state=>state.feedback.fetched)
    if(!fetched) {
        console.log("preloading...")
        store.dispatch(setStartupFeedback(feedback))

    }
    return null
}

export default Preloader