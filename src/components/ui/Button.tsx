"use client"
import styled from 'styled-components';

type ButtonProps = {
    text: string;
    background?: string,
    textColor?: string,
    fontSize?: string,
}

type ButtonStylesProps = {
    $background: string,
    $textColor: string,
    $fontSize: string,
}

const S = {
    Container: styled.button<ButtonStylesProps>`
          background-color: ${props=>props.$background};
          color: ${props=>props.$textColor};
          padding: 10px 20px;
          border-radius: 5px;
          cursor: pointer;
          outline: none;
          border: 0;
          font-size: ${props=>props.$fontSize};
          font-weight: bold;
        `
}

const Button = ({
                    text,
                    background="var(--purple)",
                    textColor="var(--white)",
                    fontSize="0.8rem"
}: ButtonProps) => {
    return(
        <S.Container $background={background} $textColor={textColor} $fontSize={fontSize}>
            {text}
        </S.Container>
    )
}

export default Button