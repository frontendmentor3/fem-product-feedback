"use client"
import React from 'react';
import styled from 'styled-components';
import {devices} from '@/styles/sizes';

type CardProps = {
    children: React.ReactNode,
    shadow?: boolean,
    textColor?: string,
    background?: string,
}

type StylesProps = {
    $shadow: boolean,
    $textColor: string,
    $background: string,
}

const S = {
    Container: styled.div<StylesProps>`
      height: 100%;
      border-radius: 10px;
      color: ${props => props.$textColor};
      background: ${props => props.$background};
      width: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 20px;
      
      @media ${devices.desktop} {
        height: unset;
        box-shadow: ${props => props.$shadow ? '0px 5px 5px var(--gray-700)' : 'none'};
      }
    `
}

const Card = ({
                  children,
                  shadow = false,
                  textColor = 'var(--navy-300)',
                  background = 'var(--white)'
              }
                  : CardProps) => {
    return (
        <S.Container
            $shadow={shadow}
            $textColor={textColor}
            $background={background}
        >
            {children}
        </S.Container>
    )
}

export default Card