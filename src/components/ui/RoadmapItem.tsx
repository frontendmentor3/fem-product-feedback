"use client"
import styled from 'styled-components';

type StylesProps = {
    $bulletColor: string
}

type RoadmapItemProps = {
    bulletColor: string,
    text: string,
    count: number
}

const S = {
    Container: styled.div<StylesProps>`
          display: flex;
          justify-content: flex-start;
          gap: 15px;
          
          div:last-child {
            margin-left: auto;
          }
          
          .bullet {
            color: ${props=>props.$bulletColor};
          }
          
          .count {
            font-weight: bold;
          }
        `
}

const RoadmapItem = ({bulletColor, text, count}:RoadmapItemProps) => {

    return(
        <S.Container $bulletColor={bulletColor}>
            <div className="bullet">&#9679;</div>
            <div className="text">{text}</div>
            <div className="count">{count}</div>
        </S.Container>
    )
 }

 export default RoadmapItem