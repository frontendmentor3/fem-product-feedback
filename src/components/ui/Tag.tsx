"use client"
import styled from 'styled-components';

const S = {
    Container: styled.div`
          background-color: #f2f4ff;
          color: var(--blue);
          font-weight: bolder;
          font-size: 0.8rem;
          padding: 5px 6px;
          border-radius: 10px;
        `
}

const Tag = ({name}:{name: string}) => {
    return(
        <S.Container>
            {name}
        </S.Container>
    )
 }

 export default Tag