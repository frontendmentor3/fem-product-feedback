// TODO: Eliminate this component
"use client"
import styled from 'styled-components';
import Sidebar from '@/components/Sidebar';
import Suggestions from '@/components/Suggestions';
import {devices} from '@/styles/sizes';
import {Feedback} from '@/types/suggestions';

const S = {
    Container: styled.main`
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      gap: 30px;
      margin-top: 30px;
      min-width: 375px;
      max-width: 1440px;

      @media ${devices.desktop}{
        flex-direction: row;
        align-items: flex-start;
      }
    `
}

export default function HomePage({feedbackArray}:{feedbackArray:Feedback[]}) {

    return (
        <S.Container>
            <Sidebar/>
            <Suggestions feedbackArray={feedbackArray}/>
        </S.Container>
    )
}
