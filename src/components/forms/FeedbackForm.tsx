"use client"
import {useForm, SubmitHandler, Controller} from 'react-hook-form';
import Button from '@/components/ui/Button';
import styled from 'styled-components';
import Select from 'react-select';
import {useId} from 'react';
import axios from 'axios';

type Inputs = {
    title: string
    category: string
    description: string
}

const S = {
    Container:styled.form`      
      display: flex;
      flex-direction: column;
      width: 90%;
      background: var(--white);
      gap: 20px;
      margin: auto;
      padding: 5%;
      font-size: 0.8rem;      
      
      h1 {
        font-size: 1.125rem;
        font-weight: bold;
      }
      
      h2 {
        font-size: 0.8rem;
        font-weight: bold;
        margin-bottom: -15px;
      }
      
      p {
        font-size: 0.8rem;
        color: var(--navy-300);
      }
    `
}

//TODO: generate this from data
const categoryOptions = [
    {value: 'feature', label: 'Feature'},
    {value: 'ui', label: 'UI'},
    {value: 'ux', label: 'UX'},
    {value: 'enhancement', label: 'Enhancement'},
    {value: 'bug', label: 'Bug'},
]

const FeedbackForm = () => {
    // the unique id is to prevent react-select hydration error
    // https://github.com/JedWatson/react-select/issues/5459
    const selectId = useId()
    const {
        register,
        handleSubmit,
        control,
        watch,
        formState: {errors},
    } = useForm<Inputs>()

    const onSubmit: SubmitHandler<Inputs> = async (input) => {
        console.log("form submit: ", input)
        const {data} = await axios.post('/api/feedback',input)
        console.log(data)
    }

    return (
        <S.Container onSubmit={handleSubmit(onSubmit)}>
            <h1>Create New Feedback</h1>
            <h2>Feedback Title</h2>
            <p>Add a short, descriptive headline</p>
            <textarea {...register("title")} />
            <h2>Category</h2>
            <p>Choose a category for your feedback</p>
            <Controller control={control} render={({field : {onChange}}) =>(
                <Select
                    instanceId={selectId}
                    options={categoryOptions}
                    isSearchable= {false}
                    onChange={(selected) => onChange(selected!.value)}
                />
            )} name="category"/>

            <h2>Detail</h2>
            <h2>Feedback Detail</h2>
            <p>Include any specific comments on what should be improved, added, etc.</p>
            <textarea {...register("description")}/>
            <Button text="Add Feedback" />
        </S.Container>
    )
}

export default FeedbackForm