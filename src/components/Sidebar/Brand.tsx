"use client"
import styled from 'styled-components';
import Card from '@/components/ui/Card';
import {devices} from '@/styles/sizes';
const S = {
    Container: styled.div`
      display: flex;
      flex-direction: column;
      justify-content: flex-end;
      align-self: flex-start;
      height: 100%;
      
      h1{
        font-size: 1rem;
        color: var(--white);
      }
      h2{
        font-size: 0.8rem;
        font-weight: lighter;
        color: var(--white);
      }
      
      @media ${devices.desktop} {
        height: 80px;
      }
    `
}

const Brand = () => {

    return(
        <Card
            background="radial-gradient(128.88% 128.88% at 103.9% -10.39%, #E84D70 0%, #A337F6 53.09%, #28A7ED 100%)"
            textColor="var(--white)"
        >
            <S.Container>
                <h1>
                    Frontend Mentor
                </h1>
                <h2>
                    Feedback Board
                </h2>
            </S.Container>
        </Card>
    )
 }
 
 export default Brand