"use client"
import RoadmapItem from '@/components/ui/RoadmapItem';
import Card from '@/components/ui/Card';
import styled from 'styled-components';
import data from '@/data/roadmap.json'
import Link from 'next/link';

const S = {
    Container:styled.div`
      width: 100%;
      height: 100%;
      header {
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
        margin-bottom: 20px;
        
        a{
          font-size: 0.8rem;
          font-weight: bolder;
          color: var(--blue);
        }
      }
    `
}

const Roadmap = () => {
    return(
        <Card>
            <S.Container>
                <header>
                    <h1>Roadmap</h1>
                    <Link href="#">View</Link>
                </header>

                {data.categories.map(c=>(
                    <RoadmapItem
                        key={c.display}
                        bulletColor={c.color}
                        text={c.display}
                        count={2}
                    />)
                )}

            </S.Container>
        </Card>
    )
 }

 export default Roadmap