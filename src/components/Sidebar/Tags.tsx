"use client"
import Tag from '@/components/ui/Tag';
import styled from 'styled-components';
import Card from '@/components/ui/Card';

const S = {
    Container: styled.div`
      display: flex;
      flex-wrap: wrap;
      align-items: flex-start;
      align-content: flex-start;
      gap: 10px;
    `
}

const Tags = () => {

    return(
        <Card shadow={true}>
            <S.Container>
                <Tag name="All"/>
                <Tag name="UI"/>
                <Tag name="UX"/>
                <Tag name="Enhancement"/>
                <Tag name="Bug"/>
                <Tag name="Feature"/>
            </S.Container>
        </Card>
    )
 }

 export default Tags