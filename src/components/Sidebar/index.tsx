"use client"
import Tags from '@/components/Sidebar/Tags';
import styled from 'styled-components';
import Brand from '@/components/Sidebar/Brand';
import Roadmap from '@/components/Sidebar/Roadmap';
import {devices} from '@/styles/sizes';

const S = {
    Container: styled.nav`
      display: none;
      flex-direction: column;
      align-items: center;
      gap: 20px;
      width: 20%;
      min-width: 220px;      
      
      @media ${devices.tablet}{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        width: 90%;
        height: 150px;
      }
      
      @media ${devices.desktop}{
        flex-direction: column;
        justify-content: flex-start;
        width: 20%;
        height: 100vh;
      }
    `
}

const Sidebar = () => {

    return(
        <S.Container>
            <Brand/>
            <Tags/>
            <Roadmap />
        </S.Container>
    )
 }

 export default Sidebar