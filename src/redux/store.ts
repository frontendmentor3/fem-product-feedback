import {configureStore} from '@reduxjs/toolkit'
import authReducer from '@/redux/features/authSlice'
import feedbackReducer from '@/redux/features/feedbackSlice'

export const store = configureStore({
    reducer: {
        auth: authReducer,
        feedback: feedbackReducer
    },
    devTools: process.env.NODE_ENV !== 'production',
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;