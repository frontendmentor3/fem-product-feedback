import {createSlice, PayloadAction} from '@reduxjs/toolkit'

type InitialStateType = {
    value: AuthState;
}

type AuthState = {
    isAuth: boolean;
    username: string;
    uid: string;
    isAdmin: boolean
}

const initialState = {
    value: {
        isAuth: false,
        username: '',
        uid: '',
        isAdmin: false
    } as AuthState,
} as InitialStateType;

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        logIn: (_state, action: PayloadAction<string>) => {
            return {
                value: {
                    isAuth: true,
                    username: action.payload,
                    uid: "",
                    isAdmin: false
                }
            }
        },
        logOut: () => {
            return initialState;
        },
    }
})

export const { logIn, logOut } = authSlice.actions;
export default authSlice.reducer;