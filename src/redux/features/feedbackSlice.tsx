import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit"
import {Feedback} from '@/types/suggestions';
import {getAllFeedback} from '@/services/feedback'


type FeedbackStateType = {
    feedback: Feedback[];
    fetched: boolean;
}

const initialState = {
    feedback: [],
    fetched: false
} as FeedbackStateType;

export const feedbackSlice = createSlice({
    name: "feedback",
    initialState,
    reducers: {
        setStartupFeedback: (state, action: PayloadAction<Feedback[]>) => {
            state.feedback = action.payload
            state.fetched = true
        }
    }
})

export const {setStartupFeedback} = feedbackSlice.actions
export default feedbackSlice.reducer

// export const selectAllFeedback = (state:FeedbackStateType) => state.feedback
// export const selectFeedbackById = (state :FeedbackStateType, feedbackId:number) =>
//    state.feedback.find(feedback => feedback.id === feedbackId)

