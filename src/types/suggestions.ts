export type Feedback = {
    id: number;
    userId: number;
    title: string;
    category: string;
    upvotes: number;
    status: string;
    description: string;
    createdAt: Date;
}