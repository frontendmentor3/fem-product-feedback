export async function getAllFeedback() {
    const data = await fetch(`${process.env.NEXT_PUBLIC_API_BASEURL}/api/feedback`)

    if(!data.ok) {
        throw new Error(`Failed to fetch data`)
    }

    return data.json()
}