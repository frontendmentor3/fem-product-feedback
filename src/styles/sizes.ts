const SCREEN_SIZES = {
    tablet: '600px',
    desktop: '900px',
    maxWidth: '1400px'
}

export const devices = {
    tablet: `(min-width: ${SCREEN_SIZES.tablet})`,
    desktop: `(min-width: ${SCREEN_SIZES.desktop})`
}