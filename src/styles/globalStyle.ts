import {createGlobalStyle} from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  :root {
    --max-width: 1100px;
    --border-radius: 12px;
    //font-family: 'Jost', sans-serif;
    --purple: #AD1FEA;    
    --ornage: #F49F85;
    --light-blue: #62BCFA;
    --blue: #4661E6;
    --navy-300: #647196;    
    --navy-500: #3A4374;
    --navy-600: #373F68;
    --gray-700: #979797;
    --gray-200: #F2F4FF;
    --gray-100: #F7F8FD;
    --white: #FFFFFF;  
    --black: #000000;
    --background: #F2F2F2;
  }

  /*
  @media (prefers-color-scheme: dark) {
    :root {
      
    }
  }
  */

  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    font-size: 16px;
  }

  html,
  body {
    max-width: 100vw;
    overflow-x: hidden;
    background-color: var(--background);
  }
  
  p {
    color: var(--navy-300);
  }
  
  h1, h2, h3, h4, h5, h6 {
    color: var(--navy-500);
  }

  body {
    
  }

  a {
    
  }

  /*
  @media (prefers-color-scheme: dark) {
    html {
      color-scheme: dark;
    }
  }
  */
`

