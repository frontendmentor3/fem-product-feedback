import {NextRequest, NextResponse} from 'next/server';
import {prisma} from '@/db/client';

export async function GET() {
    const data = await prisma.feedback.findMany()
    return new Response(JSON.stringify(data),{
        status: 200
    })
}

export async function POST(req: NextRequest,res: NextResponse) {
    const {title, category, description} = await req.json()
    const newFeedback = await prisma.feedback.create({
        data: {
            title,
            category,
            description,
            status: 'suggestion',
            userId: 1
        }
    })
    return new Response(JSON.stringify(newFeedback),{
        status: 201,
    })
}

