"use client"
import StyledComponentsRegistry from '@/lib/registry'
import {GlobalStyle} from '@/styles/globalStyle';
import {Jost} from 'next/font/google'
import {Providers} from "@/components/Providers";


const jost = Jost({
    weight: ["400","600"],
    subsets: ['latin']
})


export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <html className={`${jost.className}`}>
        <GlobalStyle/>
            <body>
                <Providers>
                    <StyledComponentsRegistry>
                        {children}
                    </StyledComponentsRegistry>
                </Providers>
            </body>
        </html>
    )
}