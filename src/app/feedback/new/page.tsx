"use client"
import FeedbackForm from '@/components/forms/FeedbackForm';
import Link from "next/link";

const AddFeedbackPage = () => {
    return(
        <div>
            <FeedbackForm />
            <Link href="/">Home</Link>
        </div>
    )
 }
 
 export default AddFeedbackPage