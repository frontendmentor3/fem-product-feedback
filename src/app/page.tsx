"use client"
import { use } from "react"
import Sidebar from "@/components/Sidebar";
import Suggestions from "@/components/Suggestions";
import styled from "styled-components";
import {devices} from "@/styles/sizes";
import Preloader from "@/components/feedback/Preloader";
import {getAllFeedback} from "@/services/feedback";
import {Feedback} from "@/types/suggestions";
import {useAppSelector} from "@/hooks/redux";

const S = {
    Container: styled.main`
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      gap: 30px;
      margin-top: 30px;
      min-width: 375px;
      max-width: 1440px;

      @media ${devices.desktop}{
        flex-direction: row;
        align-items: flex-start;
      }
    `
}

const fetchFeedback = async () => {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_BASEURL}/api/feedback`)
    return res.json()
}
const feedbackPromise = fetchFeedback()

export default function Home() {
    const feedback = use(feedbackPromise)
    return (
        <S.Container>
            <Preloader feedback={feedback}/>
            <Sidebar/>
            <Suggestions/>
        </S.Container>
    )
}
