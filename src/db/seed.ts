const {PrismaClient} = require('@prisma/client')

const prisma = new PrismaClient()

async function main() {
    const zena = await prisma.user.upsert({
        where: { username: 'velvetround'},
        update: {},
        create: {
            image: './assets/user-images/image-zena.jpg',
            name: 'Zena Kelley',
            username: 'velvetround',
            feedback: {
                create:[{
                    title: 'Add tags for solutions',
                    category: 'enhancement',
                    upvotes: 112,
                    status: 'suggestion',
                    description: 'Easier to search for solutions based on a specific stack.',
                },
                {
                    title: 'Q&A within the challenge hubs',
                    category: 'feature',
                    upvotes: 65,
                    status: 'suggestion',
                    description: 'Challenge-specific Q&A would make for easy reference.',

                }]
            }
        }
    })
}

main()
    .then(async () => {
        await prisma.$disconnect()
    })
.catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
})