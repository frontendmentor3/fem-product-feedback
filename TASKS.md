# TODO
- [ ] Redux for feedback 
- [ ] redux for auth

# Issues
- [ ] when accessing the store from the suggestion page, and fetch, it creates an infinite render/fetch loop
- [ ] moved it to `src/app/page.tsx`, there was an error with redux
- [ ] app crash when using back button to go back to main page
  - https://github.com/vercel/next.js/issues/50382
- 

# Resources
- https://redux.js.org/tutorials/essentials/part-5-async-logic